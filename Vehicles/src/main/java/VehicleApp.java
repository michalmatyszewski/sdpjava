import org.apache.logging.log4j.LogManager;

import java.util.logging.Logger;
import java.util.*;


public class VehicleApp {
    private final Logger logger = Logger.getLogger(VehicleApp.class.getName());
    List<Vehicle> vehicles = new ArrayList<>();
    private Vehicle findHighest(String type){
        List<Vehicle> list;
        if(Objects.equals(type, "ALL")){
            list = vehicles;
        }else{
            list = findAll(type);
        }

    Vehicle highest = list.get(0);

        for(int i=1; i < list.size();i++) {
            if (list.get(i).getMaxSpeed() > highest.getMaxSpeed()){
                highest = list.get(i);
            }

        }
        return highest;
    }

    private List<Vehicle> findAll(String type){

        List<Vehicle> vehic = new ArrayList<>();
        for(int i =0;i < vehicles.size();i++){
            if (Objects.equals(vehicles.get(i).getType(), type)){
                vehic.add(vehicles.get(i));
            }
        }
        return vehic;
    }


    public void run(){
        vehicles.add(new Car("BMW",270,"CAR"));
        vehicles.add(new Car("Audi",250,"CAR"));
        vehicles.add(new Bicycle("Santa Cruz",50,"BICYCLE"));
        vehicles.add(new Bicycle("Yeti",45,"BICYCLE"));
        vehicles.add(new Plane("Beoing",876,"PLANE"));
        vehicles.add(new Plane("Tupolew",960,"PLANE"));
        vehicles.add(new Ship("DisneyDream",45,"SHIP"));
        vehicles.add(new Ship("Quantum",41,"SHIP"));

        Scanner scan = new Scanner(System.in);

        String command = "";
        while (!command.equals("EXIT")) {
            System.out.println("Podaj pojazd : PLANE, CAR, BICYCLE, SHIP, ALL lub EXIT aby wyjść");
            command = scan.nextLine();
            command = command.toUpperCase(Locale.ROOT);
            switch (command){
                case "PLANE":
                case "CAR" :
                case "BICYCLE":
                case "SHIP":
                case "ALL":
                    Vehicle veh = findHighest(command);
                    logger.info("Pojazd "+ veh.getType()+" producenta " + veh.getProducer()+ " jest najszybszy (maksymalna prędkość to " + veh.getMaxSpeed() +")");
                    break;
                case "EXIT":
                    break;
                default:
                    logger.info("Podaj prawidłową opcję");
            }

        }
    }
}
