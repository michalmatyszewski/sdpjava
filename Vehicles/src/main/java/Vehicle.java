public class Vehicle {

    private String producer;
    private int maxSpeed;
    private String type;

    public Vehicle(String producer, int maxSpeed, String type) {
        this.producer = producer;
        this.maxSpeed = maxSpeed;
        this.type = type;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
